# Video.js

![](https://image.slidesharecdn.com/presentation-for-slideshare-110328232826-phpapp01/95/videojs-how-to-build-and-html5-video-player-1-728.jpg?cb=1301355613) 

## <u>Video.js Documentation</u>
[//]: # (TOC Begin)
<!-- Start Document Outline -->
* [Usage](#usage)

* [Video.js Documentation](#videojs-documentation)

* [Information on the grunt process](#information-on-the-grunt-process)
<!-- End Document Outline -->
[//]: # (TOC End)

This repo contains ***<u>ALL</u>*** Video.js repositories.

A grunt process that clones the video.js repo and generates the documentation:

* Guides are copied and converted from markdown to html

* Examples are copied

* API docs are generated from the video.js source code comments

## <u>Usage</u>

`git clone https://github.com/WeilerWebServices/Video.js.git`

` cd Video.js/video.js/`

## <u>Information on the grunt process</u>

* Dependencies: in the top level directory, run:
    `npm install`

View the package to see what the dependencies are.

* The plugins are the standard JSDoc plugins <u>EXCEPT</u> **commentsOnly.js**. This creates the files from which the docs are actually built. The plugin has been modified to leave blank lines where actual code exists in the source instead of removing all non-comment lines.

* The **createFiles** task unconditionally overwrites existing files, which is intended.

* The **Gruntfile.js** can be executed by simply using **grunt** at the command line.
